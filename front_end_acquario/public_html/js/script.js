function getUrlVars() {
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
		vars[key] = value;
	});
	return vars;
}

function getStatus(user) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "http://flockinapp.com/reef/getStatus/" + user, true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var status = JSON.parse(xhr.responseText);
            statoSensore(".lastlucesens", Math.round(status.lastlucesens*10)/10, 40, 100, "%");
            var min, max;
            if (status.atype == "Dolce") {
                min = 6.8;
                max = 7;
            }
            else {
                min = 8.2;
                max = 8.5;
            }
            statoSensore(".lastph", Math.round(status.lastph*10)/10, min, max, " ");
            statoSensore(".lasttemperaturein", Math.round(status.lasttemperaturein*10)/10, 20, 35, "° C");
            statoSensore(".lasttemperatureout", Math.round(status.lasttemperatureout*10)/10, 20, 35, "° C");
        }
    };
    xhr.send();
}

function sendCommand(user, cmdtype, command) {
    var cmd = {
        ID: user,
        cmdtype: cmdtype,
        command: command ? "ON" : "OFF"
    };

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://flockinapp.com/reef/sendCommand", true);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(JSON.stringify(cmd));
}

function statoSensore(classe, sensore, min, max, scala) {
    var colore;
    if (sensore >= min && sensore <= max) {
        colore = "lightgreen";
    }
    else {
        colore = "red";
    }
    $(classe).parent().css('borderColor', colore);
    $(classe).parent().css('color', colore);
    $(classe).parent().parent().children(".service-title").css("color", colore);
    $(classe).parent().hover(
            function() {
                $(this).css("background", colore);
                $(this).css("color", "white");
            },
            function() {
                $(this).css("background", "white");
                $(this).css("color", colore);
            }
    );
    $(classe).html(sensore + " " + scala);
}

function getGraphData(user, type) {
    var typeCmd;
    switch(type) {
        case "1":
            typeCmd="getTempMeasurementIn";
            $("#title").html("TEMPERATURA INTERNA");
            break;
        case "2":
            typeCmd="getPh";
            $("#title").html("PH");
            break;
        case "3":
            typeCmd="getLightSensor";
            $("#title").html("LUMINOSITÀ");
            break;
        case "4":
            typeCmd="getTempMeasurementOut";
            $("#title").html("TEMPERATURA ESTERNA");
    }
    var url = "http://flockinapp.com/reef/" + typeCmd + "/" + user;
    console.log(url);
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.onreadystatechange = function() {
        drawGraphData(JSON.parse(xhr.responseText));
    };
    xhr.send();
    setTimeout(function() {
        getGraphData(user, type);
    }, 5000);
}

function drawGraphData(arr) {
    var lineChartData = {
        datasets: [
            {
                label: "My First dataset",
                fillColor: "rgba(150,150,150,0.2)",
                strokeColor: "rgba(150,150,150,1)",
                pointColor: "rgba(150,150,150,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(150,150,150,1)"
            }
        ]
    };
    lineChartData.labels = [];
    lineChartData.datasets[0].data = [];
    for (var i = 10; i > 0; i--) {
        lineChartData.labels.push("");
        lineChartData.datasets[0].data.push(arr[arr.length - i].value);
    }
    var ctx = document.getElementById("canvas").getContext("2d");
    window.myLine = new Chart(ctx).Line(lineChartData, {
        responsive: true
    });
}